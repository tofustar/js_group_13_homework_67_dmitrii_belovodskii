import { Injectable } from '@angular/core';
import { Meal } from './meal.model';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()

export class MealService {
  mealsChange = new Subject<Meal[]>();
  mealsFetching = new Subject<boolean>();
  mealUploading = new Subject<boolean>();

  constructor(private http: HttpClient) {}

  private meals: Meal[] = [];

  getMeals() {
    return this.meals.slice();
  }

  fetchMeals() {
    this.mealsFetching.next(true);
    return this.http.get<{[key: string]: Meal}>('https://js-group-13-default-rtdb.firebaseio.com/meals.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const mealData = result[id];
          return new Meal(id, mealData.mealtime, mealData.mealName, mealData.calories);
        });
      }))
      .subscribe(meals => {
        this.meals = meals;
        this.mealsChange.next(this.meals.slice());
        this.mealsFetching.next(false);
      }, () => {
        this.mealsFetching.next(false);
      });
  }

  fetchMeal(id:string) {
    return this.http.get<Meal | null>(`https://js-group-13-default-rtdb.firebaseio.com/meals/${id}.json`).pipe(
      map(result => {
        if(!result) {
          return null;
        }
        return new Meal(id, result.mealtime, result.mealName, result.calories);
      })
    )
  }

  addMeal(meal: Meal) {
    const body = {
      mealtime: meal.mealtime,
      mealName: meal.mealName,
      calories: meal.calories,
    };

    this.mealUploading.next(true);

    return this.http.post('https://js-group-13-default-rtdb.firebaseio.com/meals.json', body).pipe(
      tap(() => {
        this.mealUploading.next(false);
      }, () => {
        this.mealUploading.next(false);
      })
    );
  }

  editMeal(meal: Meal) {
    this.mealUploading.next(true);

    const body = {
      mealtime: meal.mealtime,
      mealName: meal.mealName,
      calories: meal.calories,
    };

    return this.http.put(`https://js-group-13-default-rtdb.firebaseio.com/meals/${meal.id}.json`, body).pipe(
      tap(() => {
        this.mealUploading.next(false);
      }, () => {
        this.mealUploading.next(false);
      })
    );
  }

  removeMeal(id: string) {
    return this.http.delete(`https://js-group-13-default-rtdb.firebaseio.com/meals/${id}.json`)

  }
}
