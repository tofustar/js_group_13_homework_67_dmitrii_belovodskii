export class Meal {
  constructor(
    public id: string,
    public mealtime: string,
    public mealName: string,
    public calories: number,
  ) {}
}
