import { Component, OnDestroy, OnInit } from '@angular/core';
import { Meal } from '../shared/meal.model';
import { MealService } from '../shared/meal.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-meal',
  templateUrl: './meal.component.html',
  styleUrls: ['./meal.component.css']
})
export class MealComponent implements OnInit, OnDestroy {

  meals: Meal[] = [];
  mealsChangeSubscription!: Subscription;
  mealsFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private mealService: MealService) { }

  ngOnInit(): void {
    this.meals = this.mealService.getMeals();
    this.mealsChangeSubscription = this.mealService.mealsChange.subscribe((meals: Meal[]) => {
      this.meals = meals;
    });
    this.mealsFetchingSubscription = this.mealService.mealsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.mealService.fetchMeals();
  }

  ngOnDestroy() {
    this.mealsChangeSubscription.unsubscribe();
  }

  onRemove(id:string) {
    this.mealService.removeMeal(id).subscribe(() => {
      this.mealService.fetchMeals();
    });
  }
}
