import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Meal } from '../shared/meal.model';
import { Injectable } from '@angular/core';
import { EMPTY, Observable, of } from 'rxjs';
import { MealService } from '../shared/meal.service';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class MealResolverService implements Resolve<Meal>{

  constructor(private router: Router,private mealService: MealService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Meal> {
    const mealId = <string>route.params['id'];
    return this.mealService.fetchMeal(mealId).pipe(mergeMap(meal => {
      if (meal) {
        return of(meal);
      }

      return EMPTY;
    }))

  }
}
