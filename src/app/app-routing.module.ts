import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditMealComponent } from './edit-meal/edit-meal.component';
import { MealComponent } from './meal/meal.component';
import { MealResolverService } from './meal/meal-resolver.service';

const routes: Routes = [
  {path: '', component: MealComponent},
  {path: 'add-meal', component: EditMealComponent},
  {path: ':id/edit-meal', component: EditMealComponent, resolve: {
    meal: MealResolverService
    }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
