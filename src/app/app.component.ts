import { Component, OnInit } from '@angular/core';
import { MealService } from './shared/meal.service';
import { Meal } from './shared/meal.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  meals!: Meal[];

  constructor(private mealService: MealService) {
  }

  ngOnInit() {
    this.meals = this.mealService.getMeals();
    this.mealService.mealsChange.subscribe((meals: Meal[]) => {
      this.meals = meals;
    })
  }

  getTotalCalories() {
    return this.meals.reduce((sum, meals) => {
      return sum + meals.calories;
    }, 0);
  }
}
