import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MealService } from '../shared/meal.service';
import { Meal } from '../shared/meal.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-meal',
  templateUrl: './edit-meal.component.html',
  styleUrls: ['./edit-meal.component.css']
})
export class EditMealComponent implements OnInit {
  @ViewChild('f') mealForm!: NgForm;

  isEdit = false;
  editedId = '';

  constructor(private router: Router, private route: ActivatedRoute, private mealService: MealService) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      const meal = <Meal | null>data.meal;

      if(meal) {
        this.isEdit = true;
        this.editedId = meal.id;
        this.setFormValue({
          mealtime: meal.mealtime,
          mealName: meal.mealName,
          calories: meal.calories
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          mealtime: '',
          mealName: '',
          calories: ''
        });
      }
    })
  }

  setFormValue(value: {[key:string]: any}) {
    setTimeout(() => {
      this.mealForm.form.setValue(value);
    });
  }

  saveMeal() {
    const id = this.editedId || Math.random().toString();
    const meal = new Meal(
      id,
      this.mealForm.value.mealtime,
      this.mealForm.value.mealName,
      this.mealForm.value.calories
    );


    if(this.isEdit) {
      this.mealService.editMeal(meal).subscribe(() => {
        this.mealService.fetchMeals();
      });
    } else {
      this.mealService.addMeal(meal).subscribe(() => {
        this.mealService.fetchMeals();
        void this.router.navigate(['..']);
      });
    }
  }
}
